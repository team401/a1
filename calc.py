from scipy import stats

def calcRec(i, mat):
    nomi = mat[i][i]
    denomi = 0
    for j in range(0, len(mat[i])):
        denomi += mat[i][j]
    return float(nomi) / denomi

def calcProc(j, mat):
    nomi = mat[j][j]
    denomi = 0
    for i in range(0, len(mat)):
        denomi += mat[i][j]
    return float(nomi) / denomi

def calcAccuracy(mat):
    nomi = 0
    for i in range(0, len(mat)):
      nomi += mat[i][i]
    denomi = 0
    for i in range(0, len(mat)):
        for j in range(0, len(mat[i])):
            denomi += mat[i][j]
    return float(nomi) / denomi

if __name__ == "__main__":
    SMOAccuracyV = []
    SMO = [[[343, 207], [210, 340]], [[344, 206], [204, 346]], [[354, 196], [180, 370]], [[347, 203],[193, 357]],
           [[316, 234], [185, 365]], [[341, 209], [209, 341]], [[339, 211], [191, 359]], [[335, 215],[222, 328]],
           [[313, 237],[192, 358]], [[334, 216],[204, 346]]]
    print "weka.classifiers.functions.SMO"
    for mat in SMO:
        accuracy = calcAccuracy(mat)
        print "Accuracy: ", accuracy
        SMOAccuracyV.append(accuracy)
        print "Precision of negative emotion: ", calcProc(0, mat)
        print "Precision of positive emotion: ", calcProc(1, mat)
        print "Recall of negative emotino: ", calcRec(0, mat)
        print "Recall of positive emotion: ", calcRec(0, mat)
        print "==================================================="

    NBAccuracyV = []
    naiveBayes = [[[290, 260], [184, 366]], [[288, 262], [175, 375]], [[270, 280], [166, 384]],
                  [[294, 256],[170, 380]],[[259, 291], [182, 368]],[[282, 268], [181, 369]],
                  [[281, 269], [159, 391]], [[293, 257],[188, 362]], [[259, 291],[190, 360]], [[282, 268],[175, 375]]]
    print "weka.classifiers.bayes.NaiveBayes"
    for mat in naiveBayes:
        accuracy = calcAccuracy(mat)
        print "Accuracy: ", accuracy
        NBAccuracyV.append(accuracy)
        print "Precision of negative emotion: ", calcProc(0, mat)
        print "Precision of positive emotion: ", calcProc(1, mat)
        print "Recall of negative emotino: ", calcRec(0, mat)
        print "Recall of positive emotion: ", calcRec(0, mat)
        print "==================================================="

    treesAccuracyV = []
    trees = [[[326, 224], [210, 340]], [[323, 227], [208, 342]], [[306, 244], [201, 349]],
                  [[304, 246],[204, 346]],[[314, 236], [242, 308]],[[293, 257], [212, 338]],
                  [[292, 258], [211, 339]], [[307, 243],[238, 312]], [[291, 259],[210, 340]], [[308, 242],[224, 326]]]
    print "weka.classifiers.trees.J48"
    for mat in trees:
        accuracy = calcAccuracy(mat)
        print "Accuracy: ", accuracy
        treesAccuracyV.append(accuracy)
        print "Precision of negative emotion: ", calcProc(0, mat)
        print "Precision of positive emotion: ", calcProc(1, mat)
        print "Recall of negative emotino: ", calcRec(0, mat)
        print "Recall of positive emotion: ", calcRec(0, mat)
        print "==================================================="

    print "Paired t-test for SMO and NaiveBayes"
    pair1 = stats.ttest_rel(SMOAccuracyV, NBAccuracyV)
    print "The t-statistic is %.3f and the p-value is %.5f." % pair1

    print "Paired t-test for NaiveBayes and trees"
    pair2 = stats.ttest_rel(NBAccuracyV, treesAccuracyV)
    print "The t-statistic is %.3f and the p-value is %.5f." % pair2

    print "Paired t-test for trees and SMO"
    pair3 = stats.ttest_rel(treesAccuracyV, SMOAccuracyV)
    print "The t-statistic is %.3f and the p-value is %.5f." % pair3