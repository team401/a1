1) Accuracy result 
The accuracy results for different sizes of training data. 
** Please note that n represents the number of data points for each class ** 
Table 1
+------+--------------+
| n    | Accuracy (%) |
+------+--------------+
| 500  | 50.9749      |
+------+--------------+
| 1000 | 48.468       |
+------+--------------+
| 1500 | 50.6964      |
+------+--------------+
| 2000 | 50.4178      |
+------+--------------+
| 2500 | 48.1894      |
+------+--------------+
| 3000 | 49.8607      |
+------+--------------+
| 3500 | 49.8607      |
+------+--------------+
| 4000 | 49.8607      |
+------+--------------+
| 4500 | 53.4819      |
+------+--------------+
| 5000 | 54.3175      |
+------+--------------+
| 5500 | 57.6602      |
+------+--------------+

2) Comment 
Even though n kept growing up to 4000, the accuracy rate was kept low around 48% to 50%. 
The accuracy suddenly started to increase when n is 4000. 
Therefore, I speculate that a lot of noisy data is in the top portion of the original training data (before n = 4000). 
Hence, I reversed the order of the training data and conducted the same experiment. 
In the second experiment (Table 2), the accuracy is 60.1671%, when n is 2500.
Hence, from these two experiments, I found that more data have a higher possibility to contain good data,
but also have a higher possibility to contain noisy data. 
Therefore, reducing the noisy data is as important as having more data. 

Table 2 
+------+--------------+
| n    | Accuracy (%) |
+------+--------------+
| 500  | 53.4819      |
+------+--------------+
| 1000 | 54.8747      |
+------+--------------+
| 1500 | 56.2674      |
+------+--------------+
| 2000 | 55.9889      |
+------+--------------+
| 2500 | 60.1671      |
+------+--------------+
| 3000 | 57.6602      |
+------+--------------+
| 3500 | 53.3677      |
+------+--------------+
| 4000 | 55.4318      |
+------+--------------+
| 4500 | 52.9248      |
+------+--------------+
| 5000 | 56.2674      |
+------+--------------+
| 5500 | 57.6602      |
+------+--------------+   