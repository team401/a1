import sys
import re
import os.path
import traceback

ARFF_EXTENSION = ".arff"
# help string for usage
HELP_STRING = "[Usage] buildarff.py input_file output_file [maximum_number_of_data_points]"
# list of feature objects
FEATURE_LIST = []
# list of feature names
FEATURE_NAME_LIST = []
# dictionary to keep track of feature values for each data point)
# (e.g., if first-person-pronoun was found 2 times in the sentence,
# then the key is the first-person-pronoun and the value is 2)
FEATURE_DICT = {}
# available classes
# new class can be added in the list (e.g., 2 can be added in this list to represent neutral emotion)
# 0 represent negative emotion and 4 represent positive emotion
FEATURE_CLASS_LIST = [0, 4]
# dictionary to keep track of the number of data points collected for each class
FEATURE_CLASS_DICT = {}
PUNCTUATION_LIST = ["#", "$", ".", ",", ":", "(", ")", '"', "'"]
ADDITIONAL_COORD_CONJUNCTION = ["yet", "so"]
NOUN_TAGS = ["NN", "NNS", "NNP", "NNPS", "PRP", "PRP$"]

def createTokenListFromFiles(fList):
    """parse the files in the list named fList and collect tokens from the files
    e.g., createTokenListFromFiles([slang, slang2]) will parse a file named 'slang' and 'slang2'
    and collect all the tokens from the file and construct a list"""
    tokenList = []
    for fileName in fList:
        f = open(fileName, "r")
        for line in f:
            token = line.strip()
            if len(token) != 0:
                tokenList.append(token.lower())
    return tokenList

class Feature:
    '''class to represent a feature'''
    def __init__(self, name, featureType, fList, tList, literalVals, checkFunc):
        self.name = name
        self.tokensFromFile = createTokenListFromFiles(fList)
        self.tags = tList
        self.literalValues = literalVals
        self.customCheckFunction = checkFunc
        self.type = featureType

    def isTagIncluded(self, tagName):
        """Return true if and only if the POS tag name is included in this feature
        e.g., isTagIncluded('(') returns True for the feature named 'Parentheses'"""
        for i in range(0, len(self.tags)):
            if (self.tags[i].tagName.lower() == tagName.lower()):
                return True
        # If this feature can't have specific tag, then self.tag is empty
        if (len(self.tags) == 0):
            return True
        return False

    def isTagAbsolute(self, tagName):
        """Return true if and only if the token with the POS tag name is absolutely classified as this feature
        e.g., token with POS '(' can be classified as 'Parentheses', but a token with POS tag 'PRP' needs futher
        examination to determine if it's first person pronoun or not"""
        for i in range(0, len(self.tags)):
            if (self.tags[i].tagName.lower() == tagName.lower()):
                return self.tags[i].isAbsolute
        return False

    def isLiteralIncluded(self, token):
        """Return true if and only if the token is included in this feature's literal list. Some features contain a list
        called literal list to store literal token values that can be classified as the feature"""
        tokenLower = token.lower()
        for literalVal in self.literalValues:
            literalValLower = literalVal.literalValue.lower()
            if literalVal.canBeIncluded and tokenLower.find(literalValLower) != -1:
                return True
            if literalValLower == tokenLower:
                return True

        if tokenLower in self.tokensFromFile:
            return True
        else:
            return False

    def checkTokenIncludedWithCustomFunction(self, tokens, tags, currentIndex):
        """Call the custom check function for each feature to check if the token can be classified as this feature"""
        return self.customCheckFunction(tokens, tags, currentIndex)

class Tag:
    '''Class to store POS tag information for the feature object'''
    def __init__(self, tagName, isAbsolute):
        self.tagName = tagName
        self.isAbsolute = isAbsolute

class LiteralValue:
    '''Class to store literal values for the feature object'''
    def __init__(self, literalValue, canBeIncluded):
        self.literalValue = literalValue
        self.canBeIncluded = canBeIncluded

def validateInput(inputFileName, outputFileName, maxNumOfDataPoints, alternativeFeatureEnabledString):
    """validate user command line input"""
    if (os.path.isfile(inputFileName)):
        if not(outputFileName.endswith(ARFF_EXTENSION)):
            raise InvalidInputError("Input file name must be ended with .arff!")
        try:
            int(maxNumOfDataPoints)
            alternativeFeatureEnabled = int(alternativeFeatureEnabledString)
            if (alternativeFeatureEnabled != 0 and alternativeFeatureEnabled != 1):
                raise InvalidInputError("Alternative feature must be either 0 or 1")
            return True
        except ValueError:
            raise InvalidInputError("Maximum number of data points should be integer!")
    else:
        raise InvalidInputError("Input file doesn't exit!")

def createFeature(name, fType, fList, tList, literalVals=[], checkFunc=None):
    """creat a new feature. With this function, new features can be added dynamically (e.g., happy emoticons)"""
    f = Feature(name, fType, fList, tList, literalVals, checkFunc)
    FEATURE_NAME_LIST.append(name)
    FEATURE_LIST.append(f)

def removeFeature(name):
    """remove a feature. With this function, existing feature can be removed dynamically"""
    FEATURE_NAME_LIST.remove(name)
    upperName = name.upper()
    i = 0
    while (i < len(FEATURE_LIST)):
        if (FEATURE_LIST[i].name.upper() == upperName):
            del FEATURE_LIST[i]
        else:
            i += 1

def checkFutureVerb(tokens, tags, currentIndex):
    """check if the token can be classified as future verb"""
    # any present verb following "will" should be a future verb
    if (currentIndex > 0):
        if (tags[currentIndex].upper() == "MD" and tokens[currentIndex].lower() == "will"):
            return True

    # any present verb preceded by "going to" is a future verb
    if currentIndex >= 2:
        if (tokens[currentIndex - 2].lower() == "going"
            and tokens[currentIndex - 1].lower() == "to"):
            return True

    return False

def checkAllUpperCase(tokens, tags, currentIndex):
    """check if the token is all upper case"""
    if currentIndex < len(tokens) and currentIndex >= 0:
        currentToken = tokens[currentIndex]
        if tokens[currentIndex].isupper() and len(currentToken) > 1:
            return True
    return False

def writeHeader(outFile):
    """write the header of the arff file"""
    outFile.write("@relation twit_classification\n")
    for i in range(0, len(FEATURE_LIST)):
        outFile.write("@attribute {} {}\n".format(FEATURE_LIST[i].name, FEATURE_LIST[i].type))

    outFile.write("@attribute {} numeric\n".format("average_length_of_sentences"))
    outFile.write("@attribute {} numeric\n".format("average_length_of_tokens"))
    outFile.write("@attribute {} numeric\n".format("number_of_sentences"))

    outFile.write("@attribute class {{{}}}\n".format(",".join(str(x) for x in FEATURE_CLASS_LIST)))
    outFile.write("@data\n")

def writeDataLine(outFile, numOfTokens, numOfSentences,
                  totalTokenLength, moodClass):
    """write a line into arff file to represent a data point"""
    avgTokenLength = 0
    avgSentenceLength = 0

    if (numOfTokens != 0):
        avgTokenLength = float(totalTokenLength) / (numOfTokens)

    if (numOfSentences != 0):
        avgSentenceLength = float(numOfTokens) / numOfSentences

    outputLine = []
    for item in FEATURE_NAME_LIST:
        outputLine.append(FEATURE_DICT[item])

    featureValues = ','.join(str(x) for x in outputLine)

    outFile.write("{},{:.2f},{:.2f},{},{}".format(featureValues, avgSentenceLength, avgTokenLength, numOfSentences, moodClass) + "\n")

def checkEveryDataIsCollected(maxNumOfDataPoints):
    """check if the numbers of data points for each class are same as the maximum number data points
     specified from user input"""
    for item in FEATURE_CLASS_DICT:
        if (FEATURE_CLASS_DICT[item] < maxNumOfDataPoints):
            return False
    return True

def checkCoordConjunction(tokens, tags, currentIndex):
    """check if the token can be classified as a coordinating conjunction"""
    if currentIndex < len(tokens) and currentIndex >= 0:
        currentToken = tokens[currentIndex]
        if (currentToken in ADDITIONAL_COORD_CONJUNCTION
            and len(tags) - 1 > currentIndex
            and tags[currentIndex + 1] in NOUN_TAGS):
            return True
    return False

def checkPastVerb(tokens, tags, currentIndex):
    """ check if the token can be classified as a past verb"""
    if currentIndex < len(tokens) and currentIndex < len(tags) and currentIndex >= 0:
        currentToken = tokens[currentIndex]
        currentTag = tags[currentIndex]
        # have and the following past participle should be counted only once
        if (currentTag == "VBD"):
            if (currentToken.lower() == "have"):
                return False
            else:
                return True
        else:
            return False
    return False

def checkSadEmoticons(tokens, tags, currentIndex):
    """ check if the token can be classified as a sad emotion"""
    if currentIndex < len(tokens) and currentIndex >= 0:
        currentToken = tokens[currentIndex]
        previousToken = ""
        nextToken = ""
        if currentIndex > 0:
            previousToken = tokens[currentIndex-1]
        if currentIndex < len(tokens) - 1:
            nextToken = tokens[currentIndex+1]
    # possible combinations of sad emoticons including colons and semi colons
    #  :( ): D: :S S: >: :< /: :\ :/ (where colons can be replaced with the semi colons)
    followingMouth = ['(', "s", "S", "<", "/", "\\"]
    precedingMouth = [')', "D", "s", "S", ">", "/", "\\", "p", "P"]
    if (currentToken == ":" or currentToken == ";"):
        if (previousToken in precedingMouth or nextToken in followingMouth):
            return True
    return False

def checkHappyEmoticons(tokens, tags, currentIndex):
    """ check if the token can be classified as a happy emoticon """
    if currentIndex < len(tokens) and currentIndex >= 0:
        currentToken = tokens[currentIndex]
        previousToken = ""
        nextToken = ""
        if currentIndex > 0:
            previousToken = tokens[currentIndex-1]
        if currentIndex < len(tokens) - 1:
            nextToken = tokens[currentIndex+1]
    # possible combinations of happy emoticons including colons and semi colons
    #  :) (: :D where colons can be replaced with the semi colons)
    followingMouth = [')', "]", "D"]
    precedingMouth = ['(', "["]
    if (currentToken == ":" or currentToken == ";"):
        if (previousToken in precedingMouth or nextToken in followingMouth):
            return True
    return False

def writeARFF(inputFileName, outputFileName, maxNumOfDataPoints):
    """ write ARFF file named outputFileName"""
    inf = open(inputFileName, 'r')
    outf = open(outputFileName, 'w')
    writeHeader(outf)

    classPattern = re.compile("<A=\d>")
    tokenPattern = re.compile("(.*)/(.*)")

    numOfTokens = 0
    numOfSentences = 0
    totalTokenLength = 0

    skipTwitt = False

    numOfLines = 0

    for line in inf:
        numOfLines += 1
        # new tweet
        if classPattern.match(line):
            if numOfSentences > 0 and skipTwitt == False:
                writeDataLine(outf, numOfTokens, numOfSentences,
                  totalTokenLength, moodClass)

            skipTwitt = False
            numOfSentences = 0
            numOfTokens = 0
            totalTokenLength = 0
            for item in FEATURE_DICT:
                FEATURE_DICT[item] = 0

            moodClass = int(line[3])

            if (moodClass not in FEATURE_CLASS_LIST):
                skipTwitt = True
                continue

            if maxNumOfDataPoints != -1:
                if checkEveryDataIsCollected(maxNumOfDataPoints):
                    return
                else:
                    if (moodClass in FEATURE_CLASS_LIST and FEATURE_CLASS_DICT[moodClass] < maxNumOfDataPoints):
                        FEATURE_CLASS_DICT[moodClass] += 1
                    else:
                        skipTwitt = True
        # new sentence in the same tweet
        else:
            if (skipTwitt == False and len(line.strip()) != 0):
                numOfSentences += 1
                tokensAndTags = line.split()
                tokens = []
                tags = []
                for i in range(0, len(tokensAndTags)):
                    m = re.match(tokenPattern, tokensAndTags[i])
                    token = m.group(1)
                    tag = m.group(2)
                    tokens.append(token)
                    tags.append(tag)
                    if (tag not in PUNCTUATION_LIST):
                        numOfTokens += 1
                        totalTokenLength += len(token)
                for i in range(0, len(tokensAndTags)):
                    tag = tags[i]
                    token = tokens[i]
                    for j in range(0, len(FEATURE_LIST)):
                        feature = FEATURE_LIST[j]
                        if (feature.isTagIncluded(tag)):
                            if (feature.isTagAbsolute(tag) or
                                    (feature.customCheckFunction != None and feature.customCheckFunction(tokens, tags, i)) or
                                    feature.isLiteralIncluded(token)):
                                FEATURE_DICT[FEATURE_NAME_LIST[j]] += 1

    if (numOfLines > 0):
        writeDataLine(outf, numOfTokens, numOfSentences, totalTokenLength, moodClass)

def initialize(alternativeFeatureEnabled):
    """ Initialize features, dictionary and lists """
    # Every first/second/third person pronouns, first person pornoun can be tagged as NN, PRP or PRP$
    # To make the search efficient use the tag information
    createFeature("first_person_pronouns", "numeric", ["First-person"], [Tag("PRP", False), Tag("PRP$", False), Tag("NN", False)])
    createFeature("second_person_pronouns","numeric", ["Second-person"], [Tag("PRP", False), Tag("PRP$", False), Tag("NN", False)])
    createFeature("third_person_pronouns","numeric", ["Third-person"], [Tag("PRP", False), Tag("PRP$", False), Tag("NN", False)])
    # Extract coordinating_conjunction by its tag
    createFeature("coordinating_conjunctions","numeric", [], [Tag("CC", True), Tag("RB", False)], [], checkCoordConjunction)
    createFeature("past_tense_verbs","numeric", [], [Tag("VBD", False)], [], checkPastVerb)
    createFeature("future_tense_verbs","numeric", [], [Tag("VB", False), Tag("VBG", False),Tag("VBP", False), Tag("MD", False)], [], checkFutureVerb)
    createFeature("commas","numeric", [], [Tag(",", True)], [])
    # count the semi-colons and colons in emoticons, too (This behaviour gets changed if user want to use alternative approach which is counting number of emoticons)
    createFeature("colons_semi_colons","numeric", [], [Tag(":", False), Tag("NN", False)], [LiteralValue(":", True), LiteralValue(";", True)])
    createFeature("dashes","numeric", [], [Tag(":", False), Tag("NN", False)],[LiteralValue("-", True)])
    createFeature("parentheses","numeric", [], [Tag("(", True), Tag(")", True)])
    createFeature("ellipses","numeric", [], [Tag(":", False)], [LiteralValue("...",True)])
    createFeature("common_nouns","numeric",[], [Tag("NN", True), Tag("NNS", True)])
    createFeature("proper_nouns","numeric",[], [Tag("NNP", True), Tag("NNPS", True)])
    createFeature("adverbs","numeric", [], [Tag("RB", True), Tag("RBR", True), Tag("RBS", True)])
    createFeature("wh_words","numeric", [], [Tag("WDT", True), Tag("WP", True), Tag("WP$", True), Tag("WRB", True)])
    createFeature("modern_slang_acroynms","numeric", ["Slang", "Slang2"], [], [], None)
    createFeature("words_all_in_upper_case","numeric", [], [], [], checkAllUpperCase)

    # add alternative features for bonus
    if (alternativeFeatureEnabled == 1):
        removeFeature("colons_semi_colons")
        createFeature("happy_emoticons","numeric", ["happy-emoticons"], [Tag(":", False), Tag("NN", False)], [], checkHappyEmoticons)
        createFeature("sad_emoticons","numeric", ["sad-emoticons"], [Tag(":", False), Tag("NN", False)], [], checkSadEmoticons)
        createFeature("colons_semi_colons_without_emoticns", "numeric", [], [Tag(":", False)], [LiteralValue(":", True)])


    for item in FEATURE_CLASS_LIST:
        FEATURE_CLASS_DICT[item] = 0

    for item in FEATURE_NAME_LIST:
        FEATURE_DICT[item] = 0

class InvalidInputError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

if __name__ == "__main__":

    numOfArgs = len(sys.argv)

    try:
        # validate user inputs
        if numOfArgs == 5 or numOfArgs == 4 or numOfArgs == 3:
            inputFileName = sys.argv[1]
            outputFileName = sys.argv[2]
            alternativeFeatureEnabledString = "0"
            maxNumOfDataPointsString = "-1"
            if numOfArgs > 3:
                maxNumOfDataPointsString = sys.argv[3]
                if numOfArgs == 5:
                    alternativeFeatureEnabledString = sys.argv[4]
        else:
            raise InvalidInputError("Invalid number of arguments! The number of arguments should be 2 or 3.\n")

        validateInput(inputFileName, outputFileName, maxNumOfDataPointsString, alternativeFeatureEnabledString)
        # initialize the system
        initialize(int(alternativeFeatureEnabledString))
        # write ARFF
        writeARFF(inputFileName, outputFileName, int(maxNumOfDataPointsString))
    except InvalidInputError as errorMessage:
        print errorMessage
        print HELP_STRING
    except:
        print "Unexpected Error occured:", traceback.format_exc()