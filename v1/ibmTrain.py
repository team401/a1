# ibmTrain.py
# 
# This file produces 11 classifiers using the NLClassifier IBM Service
# 
# TODO: You must fill out all of the functions in this file following 
# 		the specifications exactly. DO NOT modify the headers of any
#		functions. Doing so will cause your program to fail the autotester.
#
#		You may use whatever libraries you like (as long as they are available
#		on CDF). You may find json, request, or pycurl helpful.
#

###IMPORTS###################################
#TODO: add necessary imports
import re
import random
import json
import requests

###HELPER FUNCTIONS##########################

def convert_training_csv_to_watson_csv_format(input_csv_name, group_id, output_csv_name): 
	# Converts an existing training csv file. The output file should
	# contain only the 11,000 lines of your group's specific training set.
	#
	# Inputs:
	#	input_csv - a string containing the name of the original csv file
	#		ex. "my_file.csv"
	#
	#	output_csv - a string containing the name of the output csv file
	#		ex. "my_output_file.csv"
	#
	# Returns:
	#	None
	
	line_nums = range(group_id * 5500, (group_id + 1) * 5500)
	line_nums += range(group_id * 5500 + 800000, (group_id + 1) * 5500 + 800000)

	fout = open(output_csv_name, 'w')
	
	with open(input_csv_name) as infile:
		line_count = 0
		for line in infile:
			if len(line_nums) == 0:
				break 
			if line_count == line_nums[0]:
				parts = line.split(',', 5)
				classifier, tweet = parts[0], parts[-1]
				classifier = classifier[1:-1].strip()
				tweet = tweet[1:-2].strip()
				tweet = re.sub(r"\"", "\"\"", tweet)
				fout.write("\"{}\",{}\n".format(tweet.encode("string_escape"), classifier))
				line_nums.pop(0)
			line_count += 1

	fout.close()
	return
	
def extract_subset_from_csv_file(input_csv_file, n_lines_to_extract, output_file_prefix='ibmTrain'):
	# Extracts n_lines_to_extract lines from a given csv file and writes them to 
	# an outputfile named ibmTrain#.csv (where # is n_lines_to_extract).
	#
	# Inputs: 
	#	input_csv - a string containing the name of the original csv file from which
	#		a subset of lines will be extracted
	#		ex. "my_file.csv"
	#	
	#	n_lines_to_extract - the number of lines to extract from the csv_file, as an integer
	#		ex. 500
	#
	#	output_file_prefix - a prefix for the output csv file. If unspecified, output files 
	#		are named 'ibmTrain#.csv', where # is the input parameter n_lines_to_extract.
	#		The csv must be in the "watson" 2-column format.
	#		
	# Returns:
	#	None
	
	fout = open(output_file_prefix+str(n_lines_to_extract)+".csv", 'w')

	# Get number of lines in input file
	num_lines = sum(1 for line in open(input_csv_file))
	# Create a list of numbers from 0..num_lines in random order
	line_nums = range(num_lines)
	random.shuffle(line_nums)

	classifier_count = {}
	with open(input_csv_file) as infile:
		lines = infile.readlines()
		# Check each line in the random order
		for line_num in line_nums:
			line = lines[line_num]
			# Get the classifier, need to change this if we have more than one digit
			classifier = line[-2]
			if classifier in classifier_count.keys():
				if classifier_count[classifier] >= n_lines_to_extract:
					continue
				classifier_count[classifier] += 1
			else:
				classifier_count[classifier] = 1
			fout.write(line)

	fout.close()
	return
	
def create_classifier(username, password, n, input_file_prefix='ibmTrain'):
	# Creates a classifier using the NLClassifier service specified with username and password.
	# Training_data for the classifier provided using an existing csv file named
	# ibmTrain#.csv, where # is the input parameter n.
	#
	# Inputs:
	# 	username - username for the NLClassifier to be used, as a string
	#
	# 	password - password for the NLClassifier to be used, as a string
	#
	#	n - identification number for the input_file, as an integer
	#		ex. 500
	#
	#	input_file_prefix - a prefix for the input csv file, as a string.
	#		If unspecified data will be collected from an existing csv file 
	#		named 'ibmTrain#.csv', where # is the input parameter n.
	#		The csv must be in the "watson" 2-column format.
	#
	# Returns:
	# 	A dictionary containing the response code of the classifier call, will all the fields 
	#	specified at
	#	http://www.ibm.com/smarterplanet/us/en/ibmwatson/developercloud/natural-language-classifier/api/v1/?curl#create_classifier
	#   
	#
	# Error Handling:
	#	This function should throw an exception if the create classifier call fails for any reason
	#	or if the input csv file does not exist or cannot be read.
	#

	#curl -u "{username}":"{password}" -F training_data=@train.csv -F training_metadata="{\"language\":\"en\",\"name\":\"My Classifier\"}" "https://gateway.watsonplatform.net/natural-language-classifier/api/v1/classifiers"

	url = "https://gateway.watsonplatform.net/natural-language-classifier/api/v1/classifiers"

	with open(input_file_prefix+str(n)+'.csv', 'rb') as file:
		text = file.read()

	data = {"training_data": text,
			"training_metadata": "{\"language\":\"en\",\"name\":\"Classifier "+str(n)+"\"}"}
	result = requests.post(url, auth=(username, password), files=data)
	
	try:
		result.raise_for_status()
	except Exception as e:
		print "An error occurred when sending request: {}".format(e.message)
		exit(-1)

	dic = json.loads(result.text)
	return dic
	
if __name__ == "__main__":
	
	### STEP 1: Convert csv file into two-field watson format
	input_csv_name = '/u/cs401/A1/tweets/training.1600000.processed.noemoticon.csv'
	
	#DO NOT CHANGE THE NAME OF THIS FILE
	output_csv_name = 'training_11000_watson_style.csv'
	
	convert_training_csv_to_watson_csv_format(input_csv_name, 3, output_csv_name)
	
	
	### STEP 2: Save 11 subsets in the new format into ibmTrain#.csv files
	
	#TODO: extract all 11 subsets and write the 11 new ibmTrain#.csv files
	#
	# you should make use of the following function call:
	#
	for n_lines_to_extract in [500, 2500, 5000]:
		extract_subset_from_csv_file(output_csv_name, n_lines_to_extract)
	

	### STEP 3: Create the classifiers using Watson
	
	#TODO: Create all 11 classifiers using the csv files of the subsets produced in 
	# STEP 2
	# 
	#
	# you should make use of the following function call
	username = '4d3fcfa1-7e8c-4fd9-997d-9b5b2e04880c'
	password = 'eo1UjUYGR5Qo'
	for n in [500, 2500, 5000]:
		create_classifier(username, password, n, input_file_prefix='ibmTrain')
	
	
	