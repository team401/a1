import sys
import re
import HTMLParser
import NLPlib

group_num = -1

# Getting input arguments
if len(sys.argv) == 4:
	input_file = sys.argv[1]
	group_num = sys.argv[2]
	output_file = sys.argv[3]
elif len(sys.argv) == 3:
	input_file = sys.argv[1]
	output_file = sys.argv[2]
else:
	print "Incorrect number of arguments"
	exit(-1)

# Read entire input file
fin = open(input_file, 'r')
lines = fin.readlines()
fin.close()

# Get list of lines we need to read
if group_num != -1:
	line_nums = range(int(group_num) * 5500, (int(group_num) + 1) * 5500)
	line_nums += range(int(group_num) * 5500 + 800000, (int(group_num) + 1) * 5500 + 800000)
else:
	line_nums = range(len(lines))

if line_nums[-1] > len(lines):
	print "Please run the program without group number"
	print "Input file is too small"
	exit(-1)

fin = open("abbrev.english", 'r')
abbrevs = fin.readlines()
abbrevs = [abbrev.strip() for abbrev in abbrevs]
fin.close()

# Write formatted text to output file
fout = open(output_file, 'w')
h = HTMLParser.HTMLParser()
tagger = NLPlib.NLPlib()
for i in line_nums:
	try:
		line = lines[i]
		parts = line.split(',', 5)
		tag = parts[0]
		tweet = parts[-1]

		fout.write("<A={}>\n".format(tag[1:-1]))
		tweet = tweet[1:-2].strip()
		# fout.write("DEBUG: {}\n".format(tweet))

		# Remove HTML tags
		tweet = re.sub(r"<[^>]*>", "", tweet)
		# Convert HTML character codes to ASCII
		try:
			tweet = tweet.decode('iso-8859-1') # python default is iso-8859-1
			#tweet = tweet.decode('unicode_escape').encode('ascii','ignore') # remove special characters
		except Exception:
			pass
		
		tweet = h.unescape(tweet)
		# Remove urls starting with http and www
		if line.lower().find("http") != -1 or line.lower().find("www") != -1:
			case_insensitive_pattern = re.compile(r'((https?|www)\S+)', re.IGNORECASE)
			tweet = re.sub(case_insensitive_pattern, "", tweet)
		# Remove username @ and hashtag #
		# Assuming these must be the first character of a word, and all such occurrences are removed
		tweet = re.sub(r"(^|\s)(?:\@|\#)(\S+)", r"\1\2", tweet)
		# Split into sentences
		sentences = re.findall(r"(.*?)(?:(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?|\!)\s|$)", tweet)
		sentences = [sentence for sentence in sentences if sentence.strip() != ""]
		temp = []
		for j in range(len(sentences)):
			tokens = sentences[j].lower().split(" ")
			for abbrev in abbrevs:
				if tokens[-1] == abbrev:
					temp.append(j)
		sent = []
		for j in range(len(sentences)):
			sent.append(sentences[j])
			if j in temp and j != len(sentences)-1:
				sent[-1] = sent[-1] + " " + sentences[j+1]
				j += 1
		sentences = sent
		# Split sentences into tokens
		# Current separates every ', probably good enough
		for j in range(len(sentences)):
			if sentences[j].strip() == "":
				continue
			#fout.write("{}\n".format(sentences[j].strip()))
			tokens = re.compile(r"(\s|'s|n't|\d+[.,]\d+|\W+)").split(sentences[j])
			tokens = [token.strip() for token in tokens if token.strip() != ""]
			temp = []
			for token in tokens:
				temp += re.compile(r"\s").split(token)
			tokens = temp
			tags = tagger.tag(tokens)
			for k in range(len(tokens)):
				# fout.write("{}/{} ".format(tokens[k], tags[k]))
				fout.write("{}/{} ".format(tokens[k].encode("iso-8859-1"), tags[k]))
			fout.write("\n")
			


	except Exception:
		# Just print out the original line
		print "{}".format(i)
		fout.write("{}\n".format(parts[-1][1:-2].strip()))

fout.close()
