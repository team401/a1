import sys
import re
import os.path
import traceback

# Constants
ARFF_EXTENSION = ".arff"
HELP_STRING = "[Usage] buildarff.py input_file output_file [maximum_number_of_data_points]"
FEATURE_LIST = []
FEATURE_NAME_LIST = []
FEATURE_DICT = {}
FEATURE_CLASS_LIST = [0, 4]
FEATURE_CLASS_DICT = {}
PUNCTUATION_LIST = ["#", "$", ".", ",", ":", "(", ")", '"', "'"]
ADDITIONAL_COORD_CONJUNCTION = ["yet", "so"]
NOUN_TAGS = ["NN", "NNS", "NNP", "NNPS", "PRP", "PRP$"]

class Tag:
    def __init__(self, tagName, isAbsolute):
        self.tagName = tagName
        self.isAbsolute = isAbsolute

class LiteralValue:
    def __init__(self, literalValue, canBeIncluded):
        self.literalValue = literalValue
        self.canBeIncluded = canBeIncluded

def createTokenListFromFiles(fList):
    tokenList = []
    for fileName in fList:
        f = open(fileName, "r")
        for line in f:
            token = line.strip()
            if len(token) != 0:
                tokenList.append(token.lower())
    return tokenList

class Feature:
    def __init__(self, name, featureType, fList, tList, literalVals, checkFunc):
        self.name = name
        self.tokensFromFile = createTokenListFromFiles(fList)
        self.tags = tList
        self.literalValues = literalVals
        self.customCheckFunction = checkFunc
        self.type = featureType

    def isTagIncluded(self, tagName):
        for i in range(0, len(self.tags)):
            if (self.tags[i].tagName.lower() == tagName.lower()):
                return True
        # If this feature can't have specific tag, then self.tag is empty
        if (len(self.tags) == 0):
            return True
        return False

    def isTagAbsolute(self, tagName):
        for i in range(0, len(self.tags)):
            if (self.tags[i].tagName.lower() == tagName.lower()):
                return self.tags[i].isAbsolute
        return False

    def isLiteralIncluded(self, token):
        tokenLower = token.lower()
        for literalVal in self.literalValues:
            literalValLower = literalVal.literalValue.lower()
            if literalVal.canBeIncluded and tokenLower.find(literalValLower) != -1:
                return True
            if literalValLower == tokenLower:
                return True

        if tokenLower in self.tokensFromFile:
            return True
        else:
            return False

    def checkTokenIncludedWithCustomFunction(self, tokens, tags, currentIndex):
        return self.customCheckFunction(tokens, tags, currentIndex)

def validateInput(inputFileName, outputFileName, maxNumOfDataPoints, alternativeFeatureEnabledString):
    if (os.path.isfile(inputFileName)):
        if not(outputFileName.endswith(ARFF_EXTENSION)):
            raise InvalidInputError("Input file name must be ended with .arff!")
        try:
            int(maxNumOfDataPoints)
            alternativeFeatureEnabled = int(alternativeFeatureEnabledString)
            if (alternativeFeatureEnabled != 0 and alternativeFeatureEnabled != 1):
                raise InvalidInputError("Alternative feature must be either 0 or 1")
            return True
        except ValueError:
            raise InvalidInputError("Maximum number of data points should be integer!")
    else:
        raise InvalidInputError("Input file doesn't exit!")

def createFeature(name, fType, fList, tList, literalVals=[], checkFunc=None):
    f = Feature(name, fType, fList, tList, literalVals, checkFunc)
    FEATURE_NAME_LIST.append(name)
    FEATURE_LIST.append(f)

def removeFeature(name):
    FEATURE_NAME_LIST.remove(name)
    upperName = name.upper()
    i = 0
    while (i < len(FEATURE_LIST)):
        if (FEATURE_LIST[i].name.upper() == upperName):
            del FEATURE_LIST[i]
        else:
            i += 1

def checkFutureVerb(tokens, tags, currentIndex):
    # any present verb following "will" should be a future verb
    if (currentIndex > 0):
        if (tags[currentIndex].upper() == "MD" and tokens[currentIndex].lower() == "will"):
            return True

    # any present verb preceded by "going to" is a future verb
    if currentIndex >= 2:
        if (tokens[currentIndex - 2].lower() == "going"
            and tokens[currentIndex - 1].lower() == "to"):
            return True

    return False

def checkAllUpperCase(tokens, tags, currentIndex):
    if currentIndex < len(tokens) and currentIndex >= 0:
        currentToken = tokens[currentIndex]
        if tokens[currentIndex].isupper() and len(currentToken) > 1:
            return True
    return False

def writeHeader(outFile):
    outFile.write("@relation twit_classification\n")
    for i in range(0, len(FEATURE_LIST)):
        outFile.write("@attribute {} {}\n".format(FEATURE_LIST[i].name, FEATURE_LIST[i].type))

    outFile.write("@attribute {} numeric\n".format("average_length_of_sentences"))
    outFile.write("@attribute {} numeric\n".format("average_length_of_tokens"))
    outFile.write("@attribute {} numeric\n".format("number_of_sentences"))

    outFile.write("@attribute class {{{}}}\n".format(",".join(str(x) for x in FEATURE_CLASS_LIST)))
    outFile.write("@data\n")

def writeDataLine(outFile, numOfTokens, numOfSentences,
                  totalTokenLength, moodClass):
    avgTokenLength = 0
    avgSentenceLength = 0

    if (numOfTokens != 0):
        avgTokenLength = float(totalTokenLength) / (numOfTokens)

    if (numOfSentences != 0):
        avgSentenceLength = float(numOfTokens) / numOfSentences

    outputLine = []
    for item in FEATURE_NAME_LIST:
        outputLine.append(FEATURE_DICT[item])

    featureValues = ','.join(str(x) for x in outputLine)

    outFile.write("{},{:.2f},{:.2f},{},{}".format(featureValues, avgSentenceLength, avgTokenLength, numOfSentences, moodClass) + "\n")

def checkEveryDataIsCollected(maxNumOfDataPoints):
    for item in FEATURE_CLASS_DICT:
        if (FEATURE_CLASS_DICT[item] < maxNumOfDataPoints):
            return False
    return True

def checkCoordConjunction(tokens, tags, currentIndex):
    if currentIndex < len(tokens) and currentIndex >= 0:
        currentToken = tokens[currentIndex]
        if (currentToken in ADDITIONAL_COORD_CONJUNCTION
            and len(tags) - 1 > currentIndex
            and tags[currentIndex + 1] in NOUN_TAGS):
            return True
    return False

def checkPastVerb(tokens, tags, currentIndex):
    if currentIndex < len(tokens) and currentIndex < len(tags) and currentIndex >= 0:
        currentToken = tokens[currentIndex]
        currentTag = tags[currentIndex]
        # have and the following past participle should be counted only once
        if (currentTag == "VBD"):
            if (currentToken.lower() == "have"):
                return False
            else:
                return True
        else:
            return False
    return False

def checkSadEmoticons(tokens, tags, currentIndex):
    if currentIndex < len(tokens) and currentIndex >= 0:
        currentToken = tokens[currentIndex]
        previousToken = ""
        nextToken = ""
        if currentIndex > 0:
            previousToken = tokens[currentIndex-1]
        if currentIndex < len(tokens) - 1:
            nextToken = tokens[currentIndex+1]
    # possible combinations of sad emoticons including colons and semi colons
    #  :( ): D: :S S: >: :< /: :\ :/ (where colons can be replaced with the semi colons)
    followingMouth = ['(', "s", "S", "<", "/", "\\"]
    precedingMouth = [')', "D", "s", "S", ">", "/", "\\", "p", "P"]
    if (currentToken == ":" or currentToken == ";"):
        if (previousToken in precedingMouth or nextToken in followingMouth):
            return True
    return False

def checkHappyEmoticons(tokens, tags, currentIndex):
    if currentIndex < len(tokens) and currentIndex >= 0:
        currentToken = tokens[currentIndex]
        previousToken = ""
        nextToken = ""
        if currentIndex > 0:
            previousToken = tokens[currentIndex-1]
        if currentIndex < len(tokens) - 1:
            nextToken = tokens[currentIndex+1]
    # possible combinations of happy emoticons including colons and semi colons
    #  :) (: :D where colons can be replaced with the semi colons)
    followingMouth = [')', "]", "D"]
    precedingMouth = ['(', "["]
    if (currentToken == ":" or currentToken == ";"):
        if (previousToken in precedingMouth or nextToken in followingMouth):
            return True
    return False

def writeARFF(inputFileName, outputFileName, maxNumOfDataPoints):

    inf = open(inputFileName, 'r')
    outf = open(outputFileName, 'w')
    writeHeader(outf)

    classPattern = re.compile("<A=\d>")
    tokenPattern = re.compile("(.*)/(.*)")

    numOfTokens = 0
    numOfSentences = 0
    totalTokenLength = 0

    skipTwitt = False

    for line in inf:
        if classPattern.match(line):
            if numOfSentences > 0 and skipTwitt == False:
                writeDataLine(outf, numOfTokens, numOfSentences,
                  totalTokenLength, moodClass)

            skipTwitt = False
            numOfSentences = 0
            numOfTokens = 0
            totalTokenLength = 0
            for item in FEATURE_DICT:
                FEATURE_DICT[item] = 0

            moodClass = int(line[3])

            if (moodClass not in FEATURE_CLASS_LIST):
                skipTwitt = True
                continue

            if maxNumOfDataPoints != -1:
                if checkEveryDataIsCollected(maxNumOfDataPoints):
                    return
                else:
                    if (moodClass in FEATURE_CLASS_LIST and FEATURE_CLASS_DICT[moodClass] < maxNumOfDataPoints):
                        FEATURE_CLASS_DICT[moodClass] += 1
                    else:
                        skipTwitt = True
        else:
            if (skipTwitt == False and len(line.strip()) != 0):
                numOfSentences += 1
                tokensAndTags = line.split()
                tokens = []
                tags = []
                for i in range(0, len(tokensAndTags)):
                    m = re.match(tokenPattern, tokensAndTags[i])
                    token = m.group(1)
                    tag = m.group(2)
                    tokens.append(token)
                    tags.append(tag)
                    if (tag not in PUNCTUATION_LIST):
                        numOfTokens += 1
                        totalTokenLength += len(token)
                for i in range(0, len(tokensAndTags)):
                    tag = tags[i]
                    token = tokens[i]
                    for j in range(0, len(FEATURE_LIST)):
                        feature = FEATURE_LIST[j]
                        if (feature.isTagIncluded(tag)):
                            if (feature.isTagAbsolute(tag) or
                                    (feature.customCheckFunction != None and feature.customCheckFunction(tokens, tags, i)) or
                                    feature.isLiteralIncluded(token)):
                                if (j > len(FEATURE_NAME_LIST) -1 ):
                                    return True
                                FEATURE_DICT[FEATURE_NAME_LIST[j]] += 1

    writeDataLine(outf, numOfTokens, numOfSentences, totalTokenLength, moodClass)

def initialize(alternativeFeatureEnabled):
    # Every first/second/third person pronouns, first person pornoun can be tagged as NN, PRP or PRP$
    # To make the search efficient use the tag information
    createFeature("first_person_pronouns", "numeric", ["First-person"], [Tag("PRP", False), Tag("PRP$", False), Tag("NN", False)])
    createFeature("second_person_pronouns","numeric", ["Second-person"], [Tag("PRP", False), Tag("PRP$", False), Tag("NN", False)])
    createFeature("third_person_pronouns","numeric", ["Third-person"], [Tag("PRP", False), Tag("PRP$", False), Tag("NN", False)])
    # Extract coordinating_conjunction by its tag
    createFeature("coordinating_conjunctions","numeric", [], [Tag("CC", True), Tag("RB", False)], [], checkCoordConjunction)
    createFeature("past_tense_verbs","numeric", [], [Tag("VBD", False)], [], checkPastVerb)
    createFeature("future_tense_verbs","numeric", [], [Tag("VB", False), Tag("VBG", False),Tag("VBP", False), Tag("MD", False)], [], checkFutureVerb)
    createFeature("commas","numeric", [], [Tag(",", True)], [])
    # count the semi-colons and colons in emoticons, too (This behaviour gets changed if user want to use alternative approach which is counting number of emoticons)
    createFeature("colons_semi_colons","numeric", [], [Tag(":", False), Tag("NN", False)], [LiteralValue(":", True), LiteralValue(";", True)])
    createFeature("dashes","numeric", [], [Tag(":", False), Tag("NN", False)],[LiteralValue("-", True)])
    createFeature("parentheses","numeric", [], [Tag("(", True), Tag(")", True)])
    createFeature("ellipses","numeric", [], [Tag(":", False)], [LiteralValue("...",True)])
    createFeature("common_nouns","numeric",[], [Tag("NN", True), Tag("NNS", True)])
    createFeature("proper_nouns","numeric",[], [Tag("NNP", True), Tag("NNPS", True)])
    createFeature("adverbs","numeric", [], [Tag("RB", True), Tag("RBR", True), Tag("RBS", True)])
    createFeature("wh_words","numeric", [], [Tag("WDT", True), Tag("WP", True), Tag("WP$", True), Tag("WRB", True)])
    createFeature("modern_slang_acroynms","numeric", ["Slang", "Slang2"], [], [], None)
    createFeature("words_all_in_upper_case","numeric", [], [], [], checkAllUpperCase)

    if (alternativeFeatureEnabled == 1):
        removeFeature("colons_semi_colons")
        createFeature("happy_emoticons","numeric", ["happy-emoticons"], [Tag(":", False), Tag("NN", False)], [], checkHappyEmoticons)
        createFeature("sad_emoticons","numeric", ["sad-emoticons"], [Tag(":", False), Tag("NN", False)], [], checkSadEmoticons)
        createFeature("colons_semi_colons_without_emoticns", "numeric", [], [Tag(":", False)], [LiteralValue(":", True)])


    for item in FEATURE_CLASS_LIST:
        FEATURE_CLASS_DICT[item] = 0

    for item in FEATURE_NAME_LIST:
        FEATURE_DICT[item] = 0

class InvalidInputError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

if __name__ == "__main__":

    numOfArgs = len(sys.argv)

    try:
        if numOfArgs == 5 or numOfArgs == 4 or numOfArgs == 3:
            inputFileName = sys.argv[1]
            outputFileName = sys.argv[2]
            alternativeFeatureEnabledString = "0"
            maxNumOfDataPointsString = "-1"
            if numOfArgs > 3:
                maxNumOfDataPointsString = sys.argv[3]
                if numOfArgs == 5:
                    alternativeFeatureEnabledString = sys.argv[4]
        else:
            raise InvalidInputError("Invalid number of arguments! The number of arguments should be 2 or 3.\n")

        validateInput(inputFileName, outputFileName, maxNumOfDataPointsString, alternativeFeatureEnabledString)
        initialize(int(alternativeFeatureEnabledString))
        writeARFF(inputFileName, outputFileName, int(maxNumOfDataPointsString))
    except InvalidInputError as errorMessage:
        print errorMessage
        print HELP_STRING
    except:
        print "Unexpected Error occured:", traceback.format_exc()