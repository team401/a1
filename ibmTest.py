# -*- coding: utf-8 -*-

# ibmTest.py
# 
# This file tests all 11 classifiers using the NLClassifier IBM Service
# previously created using ibmTrain.py
# 
# TODO: You must fill out all of the functions in this file following 
#       the specifications exactly. DO NOT modify the headers of any
#       functions. Doing so will cause your program to fail the autotester.
#
#       You may use whatever libraries you like (as long as they are available
#       on CDF). You may find json, request, or pycurl helpful.
#       You may also find it helpful to reuse some of your functions from ibmTrain.py.
#
import json
import pycurl
import certifi
import urllib
from StringIO import StringIO

"""
import pdb
pdb.set_trace()
"""

class AvailableAssertError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class IncorrectFormatError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def get_classifier_ids(username,password):
    # Retrieves a list of classifier ids from a NLClassifier service 
    # an outputfile named ibmTrain#.csv (where # is n_lines_to_extract).
    #
    # Inputs: 
    #   username - username for the NLClassifier to be used, as a string
    #
    #   password - password for the NLClassifier to be used, as a string
    #
    #       
    # Returns:
    #   a list of classifier ids as strings
    #
    # Error Handling:
    #   This function should throw an exception if the classifiers call fails for any reason
    #
    
    url = "https://gateway.watsonplatform.net/natural-language-classifier/api/v1/classifiers"
    result = StringIO()

    c = pycurl.Curl()
    data = []
    c.setopt(c.CAINFO, certifi.where())
    c.setopt(c.USERAGENT, 'Curl')
    c.setopt(c.USERPWD, username+':'+password)
    c.setopt(c.URL, url)
    c.setopt(c.HTTPPOST, data)
    c.setopt(c.WRITEFUNCTION, result.write)
    c.perform()
    c.close()

    dic = json.loads(result.getvalue())
    return_list = []
    for entry in dic['classifiers']:
        return_list.append(entry['classifier_id'])

    # print return_list
    return return_list
    

def assert_all_classifiers_are_available(username, password, classifier_id_list):
    # Asserts all classifiers in the classifier_id_list are 'Available' 
    #
    # Inputs: 
    #   username - username for the NLClassifier to be used, as a string
    #
    #   password - password for the NLClassifier to be used, as a string
    #
    #   classifier_id_list - a list of classifier ids as strings
    #       
    # Returns:
    #   None
    #
    # Error Handling:
    #   This function should throw an exception if the classifiers call fails for any reason AND 
    #   It should throw an error if any classifier is NOT 'Available'
    #
    
    result = StringIO()

    c = pycurl.Curl()
    data = []
    c.setopt(c.CAINFO, certifi.where())
    c.setopt(c.USERAGENT, 'Curl')
    c.setopt(c.USERPWD, username+':'+password)
    c.setopt(c.HTTPPOST, data)

    for classifier_id in classifier_id_list:
        result = StringIO()
        c.setopt(c.WRITEFUNCTION, result.write)
        url = "https://gateway.watsonplatform.net/natural-language-classifier/api/v1/classifiers"
        c.setopt(c.URL, url+'/'+classifier_id)
        c.perform()
        dic = json.loads(result.getvalue())
        if dic['status'] != "Available":
            raise AvailableAssertError("Not all classifiers are Available")

    
    c.close()
    
    return

def classify_single_text(username,password,classifier_id,text):
    # Classifies a given text using a single classifier from an NLClassifier 
    # service
    #
    # Inputs: 
    #   username - username for the NLClassifier to be used, as a string
    #
    #   password - password for the NLClassifier to be used, as a string
    #
    #   classifier_id - a classifier id, as a string
    #       
    #   text - a string of text to be classified, not UTF-8 encoded
    #       ex. "Oh, look a tweet!"
    #
    # Returns:
    #   A "classification". Aka: 
    #   a dictionary containing the top_class and the confidences of all the possible classes 
    #   Format example:
    #       {'top_class': 'class_name',
    #        'classes': [
    #                     {'class_name': 'myclass', 'confidence': 0.999} ,
    #                     {'class_name': 'myclass2', 'confidence': 0.001}
    #                   ]
    #       }
    #
    # Error Handling:
    #   This function should throw an exception if the classify call fails for any reason 
    #
    
    text = text.decode('unicode_escape').encode('ascii','ignore')
    text = urllib.quote(text)
    url = "https://gateway.watsonplatform.net/natural-language-classifier/api/v1/classifiers"
    result = StringIO()

    c = pycurl.Curl()
    data = []
    c.setopt(c.CAINFO, certifi.where())
    c.setopt(c.USERAGENT, 'Curl')
    c.setopt(c.USERPWD, username+':'+password)
    c.setopt(c.URL, url+'/'+classifier_id+'/classify?text='+text)
    c.setopt(c.HTTPPOST, data)
    c.setopt(c.WRITEFUNCTION, result.write)
    c.perform()
    c.close()

    #curl -G -u "{username}":"{password}" "https://gateway.watsonplatform.net/natural-language-classifier/api/v1/classifiers/10D41B-nlc-1/classify?text=How%20hot%20will%20it%20be%20today%3F"
    
    dic = json.loads(result.getvalue())
    dic.pop("classifier_id", None)
    dic.pop("url", None)
    dic.pop("text", None)
    return dic


def classify_all_texts(username,password,input_csv_name):
        # Classifies all texts in an input csv file using all classifiers for a given NLClassifier
        # service.
        #
        # Inputs:
        #       username - username for the NLClassifier to be used, as a string
        #
        #       password - password for the NLClassifier to be used, as a string
        #      
        #       input_csv_name - full path and name of an input csv file in the 
        #              6 column format of the input test/training files
        #
        # Returns:
        #       A dictionary of lists of "classifications".
        #       Each dictionary key is the name of a classifier.
        #       Each dictionary value is a list of "classifications" where a
        #       "classification" is in the same format as returned by
        #       classify_single_text.
        #       Each element in the main dictionary is:
        #       A list of dictionaries, one for each text, in order of lines in the
        #       input file. Each element is a dictionary containing the top_class
        #       and the confidences of all the possible classes (ie the same
        #       format as returned by classify_single_text)
        #       Format example:
        #              {‘classifiername’:
        #                      [
        #                              {'top_class': 'class_name',
        #                              'classes': [
        #                                        {'class_name': 'myclass', 'confidence': 0.999} ,
        #                                         {'class_name': 'myclass2', 'confidence': 0.001}
        #                                          ]
        #                              },
        #                              {'top_class': 'class_name',
        #                              ...
        #                              }
        #                      ]
        #              , ‘classifiername2’:
        #                      [
        #                      …      
        #                      ]
        #              …
        #              }
        #
        # Error Handling:
        #       This function should throw an exception if the classify call fails for any reason
        #       or if the input csv file is of an improper format.
        #
        classifier_id_list = get_classifier_ids(username, password)

        dic = {}
        for classifier_id in classifier_id_list:
            dic[classifier_id] = []

        with open(input_csv_name) as infile:
            for line in infile:
                parts = line.split(',', 5)
                if len(parts) < 6:
                    raise IncorrectFormatError("Incorrect Format")
                tweet = parts[-1]
                tweet = tweet[1:-2].strip()
                for classifier_id in classifier_id_list:
                    dic[classifier_id].append(classify_single_text(username, password, classifier_id, tweet))

        return dic


def compute_accuracy_of_single_classifier(classifier_dict, input_csv_file_name):
    # Given a list of "classifications" for a given classifier, compute the accuracy of this
    # classifier according to the input csv file
    #
    # Inputs:
    #   classifier_dict - A list of "classifications". Aka:
    #       A list of dictionaries, one for each text, in order of lines in the 
    #       input file. Each element is a dictionary containing the top_class
    #       and the confidences of all the possible classes (ie the same
    #       format as returned by classify_single_text)     
    #       Format example:
    #           [
    #               {'top_class': 'class_name',
    #                'classes': [
    #                           {'class_name': 'myclass', 'confidence': 0.999} ,
    #                           {'class_name': 'myclass2', 'confidence': 0.001}
    #                           ]
    #               },
    #               {'top_class': 'class_name',
    #               ...
    #               }
    #           ]
    #
    #   input_csv_name - full path and name of an input csv file in the  
    #       6 column format of the input test/training files
    #
    # Returns:
    #   The accuracy of the classifier, as a fraction between [0.0-1.0] (ie percentage/100). \
    #   See the handout for more info.
    #
    # Error Handling:
    #   This function should throw an error if there is an issue with the 
    #   inputs.
    #
    
    #TODO: fill in this function
    line_count = 0
    correct_count = 0
    with open(input_csv_file_name) as infile:
        for line in infile:
            parts = line.split(',', 5)
            if len(parts) < 6:
                raise IncorrectFormatError("Incorrect Format")
            tag = parts[0]
            tag = tag[1:-1]
            tweet = parts[-1]
            tweet = tweet[1:-2].strip()
            if int(tag) == int(classifier_dict[line_count]['top_class']):
                correct_count += 1

            line_count += 1

    if line_count != len(classifier_dict):
        raise IncorrectFormatError("Classifier dictionary and input file does not match")
    
    return float(correct_count) / line_count

def compute_average_confidence_of_single_classifier(classifier_dict, input_csv_file_name):
    # Given a list of "classifications" for a given classifier, compute the average 
    # confidence of this classifier wrt the selected class, according to the input
    # csv file. 
    #
    # Inputs:
    #   classifier_dict - A list of "classifications". Aka:
    #       A list of dictionaries, one for each text, in order of lines in the 
    #       input file. Each element is a dictionary containing the top_class
    #       and the confidences of all the possible classes (ie the same
    #       format as returned by classify_single_text)     
    #       Format example:
    #           [
    #               {'top_class': 'class_name',
    #                'classes': [
    #                           {'class_name': 'myclass', 'confidence': 0.999} ,
    #                           {'class_name': 'myclass2', 'confidence': 0.001}
    #                           ]
    #               },
    #               {'top_class': 'class_name',
    #               ...
    #               }
    #           ]
    #
    #   input_csv_name - full path and name of an input csv file in the  
    #       6 column format of the input test/training files
    #
    # Returns:
    #   The average confidence of the classifier, as a number between [0.0-1.0]
    #   See the handout for more info.
    #
    # Error Handling:
    #   This function should throw an error if there is an issue with the 
    #   inputs.
    #
    
    line_count = 0

    confidence_correct = []
    confidence_incorrect = []

    with open(input_csv_file_name) as infile:
        for line in infile:
            parts = line.split(',', 5)
            if len(parts) < 6:
                raise IncorrectFormatError("Incorrect Format")
            tag = parts[0]
            tag = tag[1:-1]
            tweet = parts[-1]
            tweet = tweet[1:-2].strip()
            
            classes = classifier_dict[line_count]['classes']
            if int(tag) == int(classifier_dict[line_count]['top_class']):
                confidence_correct.append(float(classes[0]['confidence']))
            else:
                confidence_incorrect.append(float(classes[0]['confidence']))

            line_count += 1

    if line_count != len(classifier_dict):
        raise IncorrectFormatError("Classifier dictionary and input file does not match")
    
    mean_correct_confidence = -1
    mean_incorrect_confidence = -1
    if len(confidence_correct) != 0:
        mean_correct_confidence = sum(confidence_correct) / float(len(confidence_correct))
    if len(confidence_incorrect) != 0:
        mean_incorrect_confidence = sum(confidence_incorrect) / float(len(confidence_incorrect))
    
    return mean_correct_confidence, mean_incorrect_confidence


if __name__ == "__main__":

    input_test_data = 'part1/testdata.manualSUBSET.2009.06.14.csv'
    username = '4d3fcfa1-7e8c-4fd9-997d-9b5b2e04880c'
    password = 'eo1UjUYGR5Qo'
    
    #STEP 1: Ensure all 11 classifiers are ready for testing
    classifier_id_list = get_classifier_ids(username, password)
    try:
        assert_all_classifiers_are_available(username, password, classifier_id_list)
    except AvailableAssertError:
        print "Not all classifiers are available"
        exit(-1)

    #STEP 2: Test the test data on all classifiers
    try:
        classifier_dict = classify_all_texts(username, password, input_test_data)
    except IncorrectFormatError:
        print "Incorrect input format"
        exit(-2)

    #STEP 3: Compute the accuracy for each classifier
    try:
        for key in classifier_dict.keys():
            print compute_accuracy_of_single_classifier(classifier_dict[key], input_test_data)
    except IncorrectFormatError:
        print "Step 3: Classifier dictionary and input file does not match"
        exit(-3)
    
    #STEP 4: Compute the confidence of each class for each classifier
    print "Outputs for Step 4:"
    print "Format: Confidence for: (correct, incorrect) prediction"
    try:
        for key in classifier_dict.keys():
            print compute_average_confidence_of_single_classifier(classifier_dict[key], input_test_data)
    except IncorrectFormatError:
        print "Step 4: Classifier dictionary and input file does not match"
        exit(-3)
    
